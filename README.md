Uran-R-Us
=========

TL;DR: This mod fixes Uranium Magical Crops when IndustrialCraft2 is disabled (via awful hackery).

Uran-R-Us is a terribly named, hacked together Minecraft mod to fix an issue in the Direwolf20 1.7 FTB modpack (if memory serves). Basically, if you disable IndustrialCraft2 - because it is buggy as hell - you loose all Uranium in the world. BigReactors still works, because Yellorium exists, and Yellorium is ore-dictionaried to Uranium. All good? No, Magical Crops will now no longer work. Specifically, you can no longer *craft* uranium seeds, because the crafting recipe doesn't use the ore dictionary. Doh! The seeds still work fine.

This mod uses introspection to look up certain Magical Crop items (seeds), and adds a recipe to craft with Yellorium. It also provides a recipe to craft grow uranium essence directly to Yellorium. There are also two more recipes for you to find :)

This mod is a single, giant hack. Be warned. It's not pretty, or possibly even functional. I had no idea what I was doing, how Gradle or Forge or Java development in general works and I didn't care to find out. But I'm putting it out there, so if you're ever in a similar situation, I hope this is helpful.

Toby
