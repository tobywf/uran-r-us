package uranrus;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;
import cpw.mods.fml.common.registry.GameRegistry;

import uranrus.MagicalCropsWrapper;
import uranrus.proxy.Proxy;

@Mod(modid = UranRus.ID, name = UranRus.NAME, version = UranRus.VERSION, dependencies = "after:BigReactors;after:magicalcrops")
public class UranRus {
    public static final String ID = "uranrus";
    public static final String NAME = "Uran-R-us";
    public static final String VERSION = "0.1";

    @Instance
    public static UranRus instance;

    public static final String CLIENT_PROXY = "uranrus.proxy.UniversalProxy";
    public static final String SERVER_PROXY = "uranrus.proxy.UniversalProxy";

    @SidedProxy(clientSide = CLIENT_PROXY, serverSide = SERVER_PROXY)
    public static Proxy proxy;

    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {}

    @EventHandler
    public void onInit(FMLInitializationEvent event) {
        ItemStack oreYellorite = OreDictionary.getOres("oreYellorite").get(0);
        OreDictionary.registerOre("oreUranium", oreYellorite);

        ItemStack ingotYellorite = OreDictionary.getOres("ingotYellorium").get(0);

        MagicalCropsWrapper mod_mCrops = new MagicalCropsWrapper();

        ItemStack mSeedsUranium = mod_mCrops.getItemStack("mSeedsUranium");
        ItemStack extremeEssence = mod_mCrops.getItemStack("MagicEssence", 1, 4);

        if (extremeEssence != null && mSeedsUranium != null) {
            GameRegistry.addRecipe(mSeedsUranium, new Object[] {
                "yey",
                "ese",
                "yey",
                's', Item.seeds,
                'y', ingotYellorite,
                'e', extremeEssence
            });
        }

        ItemStack uraniumEssence = mod_mCrops.getItemStack("ModEssence", 1, 15);

        if (uraniumEssence != null) {
            GameRegistry.addRecipe(oreYellorite, new Object[] {
                "xxx",
                "x x",
                "xxx",
                'x', uraniumEssence
            });
        }

        GameRegistry.addRecipe(new ItemStack(Item.netherStar, 2), new Object[] {
            "sss",
            "sxs",
            "sss",
            'x', Item.netherStar,
            's', new ItemStack(Item.skull, 1, 1)
        });

        ItemStack cowEssence = mod_mCrops.getItemStack("soulCropEssence", 1, 0);

        if (cowEssence != null) {
            GameRegistry.addRecipe(new ItemStack(Item.rottenFlesh), new Object[] {
                "xx ",
                "   ",
                "   ",
                'x', cowEssence
            });
        }
    }

    @EventHandler
    public void postInit(FMLPostInitializationEvent event) {}
}
