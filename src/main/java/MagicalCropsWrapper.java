package uranrus;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class MagicalCropsWrapper {
    private Class mod_mCrops;

    public MagicalCropsWrapper() {
       try {
            mod_mCrops = Class.forName("magicalcrops.mod_mCrops");
        } catch (ClassNotFoundException e) {
            mod_mCrops = null;
        }
    }

    private Object getField(String field) {
        if (mod_mCrops == null) {
            return null;
        }

        try {
            return mod_mCrops.getField(field).get(null);
        } catch (NoSuchFieldException e) {
            return null;
        } catch (IllegalAccessException e) {
            return null;
        }
    }

    public Item getItem(String item) {
        return (Item)getField(item);
    }

    public Block getBlock(String block) {
        return (Block)getField(block);
    }

    public ItemStack getItemStack(String item) {
        Item i = getItem(item);
        if (i == null) {
            return null;
        }
        return new ItemStack(i);
    }

    public ItemStack getItemStack(String item, Integer number, Integer id) {
        Item i = getItem(item);
        if (i == null) {
            return null;
        }
        return new ItemStack(i, number, id);
    }
}
